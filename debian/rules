#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
export DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
export DEB_HOST_ARCH       ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

ifeq ($(DEB_HOST_ARCH),sparc)
    # Targetting at older sparc's causes compilation errors. Steve Langasek
    # says this works and it is anyway the oldest sparc generation we
    # support in Debian (or kernel/glibc).
	CFLAGS += -mcpu=v8
endif

# Fix to make it build on hppa, mipsel and s390
# If this is not fixed, gcc is invoked as a cross compiler.
ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
	confflags += --build $(DEB_HOST_GNU_TYPE)
else
	confflags += --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
endif


%:
	dh $@ --with autoreconf

override_dh_auto_configure:
	CFLAGS="$(CFLAGS)" ./configure $(confflags) \
				--disable-alternative-syscalls \
				--prefix=/usr --enable-release \
				--mandir=\$${prefix}/share/man \
				--infodir=\$${prefix}/share/info \
				--libexecdir=\$${prefix}/lib \
				--datarootdir=\$${prefix}/lib \
				--sysconfdir=/etc --enable-dietlibc \
				--localstatedir=/var \
				--with-vrootdir=/var/lib/vservers \
				--disable-systemd

override_dh_auto_install:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	# Add here commands to install the package into debian/util-vserver.
	$(MAKE) $(MAKE_OPTS) DESTDIR=$(CURDIR)/debian/util-vserver install install-distribution
	# Fixes
	chmod +x $(CURDIR)/debian/util-vserver/usr/lib/util-vserver/distributions/template/initpre
	chmod +x $(CURDIR)/debian/util-vserver/usr/lib/util-vserver/distributions/template/initpost
	rm -f $(CURDIR)/debian/util-vserver/etc/init.d/vservers-legacy
	rm -f $(CURDIR)/debian/util-vserver/etc/init.d/vservers-default
	rm -f $(CURDIR)/debian/util-vserver/etc/init.d/rebootmgr
	rm -f $(CURDIR)/debian/util-vserver/etc/init.d/vprocunhide
	rm -f $(CURDIR)/debian/util-vserver/etc/vservers.conf
	# remove legacy binaries
	rm -rf $(CURDIR)/debian/util-vserver/usr/lib/util-vserver/legacy
	rm -f $(CURDIR)/debian/util-vserver/usr/sbin/vserver-copy
	# legacy man pages
	rm -f $(CURDIR)/debian/util-vserver/usr/share/man/man8/distrib-info.8
	rm -f $(CURDIR)/debian/util-vserver/usr/share/man/man8/vserver-copy.8
	rm -f $(CURDIR)/debian/util-vserver/usr/share/man/man8/rebootmgr.8
	# legacy initscripts
	rm -f $(CURDIR)/debian/util-vserver/etc/init.d/v_*
	# Fix for symlinks in /etc/vservers/.defaults
	rm -f $(CURDIR)/debian/util-vserver/etc/vservers/.defaults/vdirbase
	rm -f $(CURDIR)/debian/util-vserver/etc/vservers/.defaults/run.rev
	# Rewrite util-vserver-vars
	perl -pi -e \
	's|^(.*DEFAULT_VSERVERDIR=).*$$|$$1"/etc/vservers/.defaults/vdirbase"|;' \
		debian/util-vserver/usr/lib/util-vserver/util-vserver-vars
	# remove the var directories that are managed by the initscript and are otherwise violating
	# policy 9.3.2
	rm -rf $(CURDIR)/debian/util-vserver/var/run
	# Install bash completion
	mkdir -p $(CURDIR)/debian/util-vserver/usr/share/bash-completion/completions
	cp $(CURDIR)/debian/vserver.bash_completion $(CURDIR)/debian/util-vserver/usr/share/bash-completion/completions/vserver
	# Lintian overrides
	mkdir -p $(CURDIR)/debian/util-vserver/usr/share/lintian/overrides
	install -m644 debian/util-vserver.override $(CURDIR)/debian/util-vserver/usr/share/lintian/overrides/util-vserver
	# Clean libtool la files
	find $(CURDIR)/debian/util-vserver -type f -name "*.la" -delete

override_dh_auto_clean:
	rm -f $(CURDIR)/scripts/vshelper
	dh_auto_clean

override_dh_installinit:
	dh_installinit -r

override_dh_installdebconf:
	dh_installdebconf -n

override_dh_installchangelogs:
	dh_installchangelogs ChangeLog

override_dh_makeshlibs:
	dh_makeshlibs -Xrpm

override_dh_shlibdeps:
	dh_shlibdeps -L util-vserver -l debian/util-vserver/usr/lib

override_dh_auto_test:
	# If tests fail print the test-suite.log file, but don't abort the build
	@if ! dh_auto_test; then \
		echo "WARNING: Tests failed!"; \
		echo "------------------------------"; \
		echo " Printing file test-suite.log"; \
		echo "------------------------------"; \
		if test -f $(CURDIR)/test-suite.log; then \
			cat $(CURDIR)/test-suite.log; \
		else \
			echo "ERROR: Can't find file $(CURDIR)/test-suite.log"; \
		fi; \
		echo "------------------------------"; \
		echo "  End of file test-suite.log"; \
		echo "------------------------------"; \
	fi

override_dh_gencontrol:
	dh_gencontrol -- \
		-Vmisc:Built-Using="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W dietlibc-dev)"
