util-vserver (0.30.214-4) unstable; urgency=medium

  Versions of util-vserver up through 0.30.212 allowed you to manually
  manage context ids if you wanted to use them, enabling you to use
  dynamically allocated ones if you didn't manage them. However in 
  0.30.214 dynamic context IDs are no longer supported and you will
  receive and error if you are using them with a kernel that doesn't
  support them. The kernels that ship with Lenny do not support them
  and you will not be able to start, or even build a guest using a
  dynamic context. 

  If you get an error similar to the following when attempting to
  enter, or otherwise perform operations on your vserver guest, then
  you are using dynamic context IDs and you will need to migrate your
  guest to static contexts:

  vtag: vc_tagopt2tag(""): No such file or directory

  To create a static context ID, simply stop your guest, echo an unique
  number to /etc/vserver/<guest>/context and then start your guest.

util-vserver (0.30.211-4) unstable; urgency=high

  The behavior of /etc/default/util-vserver AUTO has changed slightly, please
  read the comments in that file. 

  All vservers will be stopped when /etc/init.d/util-vserver stop is run 
  due to the addition of the default ALWAYS_STOP=true. This is to avoid 
  unclean filesystems on shutdown, if you do not want this behavior please
  adjust your /etc/default/util-vserver settings.

 -- micah anderson <micah@debian.org>  Wed, 22 Nov 2006 21:32:22 -0700
